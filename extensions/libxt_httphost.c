#include <stdio.h>
#include <xtables.h>
#include <linux/netfilter/xt_httphost.h>

enum {
	O_HTTPHOST = 0,
};

static void httphost_help(void)
{
	printf(
"httphost match options:\n"
"[!] --httphost httphost\n"
"				Match HTTP HOST header\n");
}

#define s struct xt_httphost_info
static const struct xt_option_entry httphost_opts[] = {
	{.name = "httphost", .id = O_HTTPHOST, .type = XTTYPE_HTTPHOST,
	 .min = 2, .max = XT_HTTPHOST_MAX_HTTPHOST_SIZE - 1,
	 .flags = XTOPT_INVERT | XTOPT_PUT | XTOPT_MAND,
	 XTOPT_POINTER(s, httphost)},
	XTOPT_TABLEEND,
};
#undef s

static void httphost_parse(struct xt_option_call *cb)
{
	struct xt_httphost_info *macinfo = cb->data;

	xtables_option_parse(cb);
	if (cb->invert)
		macinfo->invert = 1;
}

static void print_httphost(const unsigned char *httphost)
{
	printf(" %s", httphost);
}

static void
httphost_print(const void *ip, const struct xt_entry_match *match, int numeric)
{
	const struct xt_httphost_info *info = (void *)match->data;

	printf(" HOST");

	if (info->invert)
		printf(" !");

	print_httphost(info->httphost);
}

static void httphost_save(const void *ip, const struct xt_entry_match *match)
{
	const struct xt_httphost_info *info = (void *)match->data;

	if (info->invert)
		printf(" !");

	printf(" --httphost");
	print_httphost(info->httphost);
}

static void print_httphost_xlate(const unsigned char *httphost,
			    struct xt_xlate *xl)
{
	xt_xlate_add(xl, "%s", httphost);
}

static int httphost_xlate(struct xt_xlate *xl,
		     const struct xt_xlate_mt_params *params)
{
	const struct xt_httphost_info *info = (void *)params->match->data;

	xt_xlate_add(xl, "httphost%s ", info->invert ? " !=" : "");
	print_httphost_xlate(info->httphost, xl);

	return 1;
}

static struct xtables_match httphost_mt_reg[] = {
	{
		.family		= NFPROTO_IPV4,
		.name		= "httphost",
		.version	= XTABLES_VERSION,
		.size		= XT_ALIGN(sizeof(struct xt_httphost_info)),
		.userspacesize	= XT_ALIGN(sizeof(struct xt_httphost_info)),
		.help		= httphost_help,
		.x6_parse	= httphost_parse,
		.print		= httphost_print,
		.save		= httphost_save,
		.x6_options	= httphost_opts,
		.xlate		= httphost_xlate,
	},
};

void _init(void)
{
	xtables_register_matches(httphost_mt_reg, ARRAY_SIZE(httphost_mt_reg));
}
